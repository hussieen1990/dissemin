import django
import json
import gzip
import os
import sys
from os import environ as env

if __name__ == '__main__':
    if not 'DJANGO_SETTINGS_MODULE' in env:
        from dissemin import settings
        env.setdefault('DJANGO_SETTINGS_MODULE', settings.__name__)

    django.setup()
    
    from backend.citeproc import CrossRef,CiteprocContainerTitleError,CiteprocAuthorError

    assert len(sys.argv) >= 2, "Usage: "+sys.argv[0]+" gzipped_json_or_dir ..."

    nb_items = 0
    nb_items_processed = 0

    for c in range(1, len(sys.argv)):
        files = sys.argv[c]

        if os.path.isdir(files):
            to_process = map(lambda f : files+"/"+f, sorted(
                os.listdir(files),
                key = lambda f : int(f.replace(".json.gz",""))))
        elif os.path.isfile(files):
            to_process = [files]
        else:
            print(files + " is neither a file nor a directory", file=sys.stderr)
            sys.exit(1)

        for f in to_process:
            print("Importing data from "+f)
            with gzip.open(f, "r") as g:
                data = g.read()
                j = json.loads(data.decode('utf-8'))
                items = j["items"]
                for i in items:
                    nb_items+=1
                    try:
                        CrossRef.to_paper(i)
                        nb_items_processed+=1
                    except CiteprocAuthorError:
                        # No list of authors, we skip this entry
                        pass
                    except CiteprocContainerTitleError:
                        # No container (journal) title, we skip this entry
                        pass

    print("Seen %d items, processed %d items, skipped %d items"%(nb_items,nb_items_processed,nb_items-nb_items_processed))
