#!/usr/bin/env python3

import json
import os
import sys

assert len(sys.argv) >= 2, "Usage: "+sys.argv[0]+" file_or_dir ..."

def process_publication(publication):
    title=publication["title"][0]["title"]
    issn = ""
    essn = ""
    legacy = ""
    for x in publication["issns"]:
        if not("issn" in x):
            continue
        if not("type") in x:
            legacy = x["issn"]
        elif x["type"] == "print" and issn == "":
            issn = x["issn"]
        elif x["type"] == "electronic" and essn == "":
            essn = x["issn"]
        elif x["type"] == "legacy" and legacy == "":
            legacy = x["issn"]
    if issn == None and legacy != None:
        issn = legacy
    if not("publisher_policy" in publication):
        publication["publisher_policy"]=[]    

    processed_publisher_policies = map(process_publisher_policy,
            publication["publisher_policy"])
    best_pp = ""
    best_pp_score = -1
    for p in processed_publisher_policies:
        pp_score = 0
        for v in range(4,7):
            if p[v] == "can":
                pp_score += 2
            elif p[v] == "restricted":
                pp_score += 1
        if p[7] == "OA":
            pp_score += 1        
        if pp_score > best_pp_score:
            best_pp=p[0]
            best_pp_score=pp_score

    return [[str(publication["id"]), title, issn, essn, \
        publication["system_metadata"]["date_modified"], best_pp], \
      map(lambda x:x["id"], publication["publisher_policy"])]

def process_publisher_policy(pp):
    versions = {
            "published": "cannot",
            "accepted": "cannot",
            "submitted": "cannot",
            }

    oa_journal = False

    if 'permitted_oa' in pp:
        for poa in pp['permitted_oa']:
            if not('article_version' in poa):
                # This is not a real permitted_oa, skip it
                continue

            restrictions = False

            if 'prequisites' in poa and \
                    (len(poa['prerequisites']['prerequisites'])>0 or \
                    len(poa['prerequisites']['prerequisites_funders']>0)):
                restrictions = True
            if "embargo" in poa:
                restrictions = True
            if "additional_oa_fee" in poa and poa["additional_oa_fee"] == "yes":
                continue

            if "location" in poa and \
                    "location" in poa["location"] and \
                    "this_journal" in poa["location"]["location"]:
                        if "article_version" in poa and "published" in poa["article_version"]:
                            oa_journal = True

            if "location" in poa and \
                    (not("location" in poa["location"]) or \
                    all(not(l in allowable_locations) for l in poa["location"]["location"])):
                        continue

            for article_version in poa['article_version']:
                if restrictions and versions[article_version] != "can":
                    versions[article_version] = "restricted"
                else:
                    versions[article_version] = "can"

    if oa_journal:
        oa_status = "OA"
    elif "can" in versions.values():
        oa_status = "OK"
    else:
        oa_status = "NOK"

    name=""
    url=""
    if "publisher" in pp:
        name = pp["publisher"]["name"][0]["name"]
        if "url" in pp["publisher"]:
            url = pp["publisher"]["url"]

    date_modified = ""
    if "system_metadata" in pp:
        date_modified = pp["system_metadata"]["date_modified"]

    return [str(pp["id"]), name, pp["internal_moniker"], url,
        versions["submitted"],
        versions["accepted"],
        versions["published"],
        oa_status,
        date_modified]

f_journals = open("journals.tsv", "w")
f_pp = open("pp.tsv", "w")
f_journals2pp = open("journals2pp.tsv", "w")

for c in range(1, len(sys.argv)):
    files = sys.argv[c]

    if os.path.isdir(files):
        to_process = map(lambda f : files+"/"+f, sorted(
                os.listdir(files),
                key = lambda f : int(f.replace(".json",""))))
    elif os.path.isfile(files):
        to_process = [files]
    else:
        print(files + " is neither a file nor a directory", file=sys.stderr)
        sys.exit(1)

    allowable_locations = set([
        'any_repository',
        'any_website',
        'institutional_repository',
        'institutional_website',
        'non_commercial_institutional_repository',
        'non_commercial_repository',
        'non_commercial_website',
        'preprint_repository'
        ])

    for f in to_process:
        print("Processing " + f, file=sys.stderr)
        fp = open(f, encoding='utf-8')
        data = json.load(fp)
        fp.close()

        for item in data:
            if "title" in item:
                result = process_publication(item)
                f_journals.write("\t".join(result[0])+"\n")
                for i in result[1]:
                    f_journals2pp.write(str(result[0][0])+"\t"+str(i)+"\n")
            elif "internal_moniker" in item:
                f_pp.write("\t".join(process_publisher_policy(item))+"\n")
            else:
                assert False, "Neither a title nor an internal_moniker in item #" + item["id"]

f_journals.close()
f_pp.close()

