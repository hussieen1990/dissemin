# Generated by Django 2.2.28 on 2022-06-24 12:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publishers', '0008_fix_name_indices'),
    ]

    operations = [
        migrations.AddField(
            model_name='journal',
            name='romeo_id',
            field=models.CharField(db_index=True, max_length=64, null=True, unique=True),
        ),
    ]
