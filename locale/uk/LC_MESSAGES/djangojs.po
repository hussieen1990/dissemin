# Translation of Dissemin papers to Ukrainian (українська)
# Exported from translatewiki.net
#
# Author: DDPAT
# Author: Ice bulldog
# --
# This file is part of the Dissemin Project
# This file is distributed under the same license as the Dissemin package.
#
msgid ""
msgstr ""
""
"PO-Revision-Date: 2022-07-28 11:03:54+0000\n"
"X-POT-Import-Date: 2021-05-26 12:39:16+0000\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"X-Generator: MediaWiki 1.39.0-alpha; Translate 2022-07-28\n"
"Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );\n"

#: website/static/js/dissemin.js:202
#, javascript-format
msgid "%s paper found"
msgid_plural "%s papers found"
msgstr[0] "%s папір знайдено"
msgstr[1] "%s паперів знайдено"
msgstr[2] ""

#: website/static/js/dissemin.js:291
msgid "Claiming..."
msgstr "Ствердження…"

#: website/static/js/dissemin.js:295
msgid "Unclaiming..."
msgstr "Відмова від заявки…"

#: website/static/js/dissemin.js:311
msgid "Claiming failed!"
msgstr "Претензія не вдалася!"

#: website/static/js/dissemin.js:314
msgid "Unclaiming failed!"
msgstr "Не вдалося заявити претензію!"

#: website/static/js/dissemin.js:319
msgid "Exclude from my profile"
msgstr "Виключити з мого профілю"

#: website/static/js/dissemin.js:323
msgid "Include in my profile"
msgstr "Включити в мій профіль"

#: website/static/js/dissemin.js:348
msgid "Adding to to-do list"
msgstr "Додавання до списку справ"

#: website/static/js/dissemin.js:352
msgid "Removing from to-do list"
msgstr "Видалення зі списку справ"

#: website/static/js/dissemin.js:598
msgid "Preview of uploaded file"
msgstr "Попередній перегляд завантаженого файлу"

#: website/static/js/dissemin.js:601
msgid "Pages"
msgstr "Сторінок"

#: website/static/js/dissemin.js:602
msgid "Size"
msgstr "Розмір"

#: website/static/js/dissemin.js:617
msgid "While uploading %(file)s the following error occured:"
msgstr "Під час завантаження %(file)s сталася така помилка:"

#: website/static/js/dissemin.js:626 website/static/js/dissemin.js:681
#: website/static/js/dissemin.js:687
msgid "Unknown error"
msgstr "Невідома помилка"

#: website/static/js/dissemin.js:671
msgid "While fetching file from %(url)s the following error occured:"
msgstr "Під час отримання файлу з %(url)s сталася така помилка:"

#: website/static/js/dissemin.js:781
msgid "Sorry, we could not fill this for you."
msgstr "На жаль, ми не змогли заповнити це для вас."

#: website/static/js/dissemin.js:782
msgid "Trying to fill this field automatically for you..."
msgstr "Спроба заповнити це поле автоматично для вас…"

#: website/static/js/dissemin.js:816
msgid "You have not selected a file for upload."
msgstr "Ви не вибрали файл для завантаження."

#: website/static/js/dissemin.js:843
msgid "Dissemin encountered an error, please try again later."
msgstr "У Dissemin сталася помилка. Повторіть спробу пізніше."

#: website/static/js/moment.min.js:1
msgid ": "
msgstr ":/"

